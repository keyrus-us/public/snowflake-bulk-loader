#################################
from ayx import Package
from ayx import Alteryx
import gzip
import shutil
import os 
import multiprocessing
import concurrent.futures
from concurrent.futures import ThreadPoolExecutor
import datetime

filepath = Alteryx.read("#1")['FilePath'][0]

def filezip_v2(path):
    with open(path, 'rb') as f:
        for chunk in iter(lambda: f.read(4096*4096), b''):
            with gzip.open(path+'.gz', 'ab',compresslevel=3) as w:
                w.write(chunk)
    os.remove(path)

directory = os.path.dirname(filepath)
files = [directory +"\\" +f for f in os.listdir(directory) if filepath in directory +"\\" +f]

start_time = datetime.datetime.now()
threads = 2
print("{0} INFO: Starting ThreadPoolExecutor with max {1} threads per CPU and compressing files into gzip at level 3 compression...".format(start_time,threads))
with concurrent.futures.ThreadPoolExecutor(max_workers=multiprocessing.cpu_count()*threads) as executor:
    executor.map(filezip_v2,files) 
end_time = datetime.datetime.now()
print("{0} INFO: Compression completed after {1}".format(end_time, end_time-start_time))
