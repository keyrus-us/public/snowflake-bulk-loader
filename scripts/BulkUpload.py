from ayx import Package
from ayx import Alteryx

import os
import sys
import datetime

## Set this to the network path of installed snowflake files OR network path of whl files
# If both of these are populated, only the snowflake_connector_source_path is considered
# If both are None, then PyPI will be the installation source
# Note that pip installer will fail if source or target path has any spaces in it!
snowflake_connector_source_path = None #"G:\\path\\to\\directory\\with\\libs"
snowflake_connector_whl_path = None #"G:\\path\\to\\whl\\files"   # Avoid spaces in this path if using .whl source repo 
#########################################################################################

## Target path # Avoid spaces in this path
libpath = os.getenv("TEMP")+os.sep+"snowflake_pycache"
sys.path.append(libpath)

# TODO: Add option to use file output for logger
timing = { "start":datetime.datetime.now() }
print("{0} INFO: Importing snowflake connector lib...".format(timing['start']))

if not os.path.exists(libpath) or not os.listdir(libpath):
    import shutil
    from shutil import copytree, ignore_patterns
    import importlib

    # Clean up local snowflake cache
    if os.path.exists(libpath):
        shutil.rmtree(libpath)

    # Copy installed files from source to libpath
    if snowflake_connector_source_path is not None:
        print("{0} WARN: Module not found locally, downloading from network location at {1}...".format(timing['start'],snowflake_connector_source_path))
        shutil.copytree(snowflake_connector_source_path, libpath, ignore=ignore_patterns('*__pycache__*'))

    # Install files from whl to libpath
    elif snowflake_connector_whl_path is not None:
        print("{0} WARN: Module not found locally, installing from whl files at {1}...".format(timing['start'],snowflake_connector_whl_path))
        Package.installPackages(package='snowflake-connector-python', install_type="install --no-index --find-links={0} --target={1}".format(snowflake_connector_whl_path, libpath))

    # Download files from PyPI and install to libpath
    else:
        print("{0} WARN: Module not found, downloading from PyPI".format(timing['start']))
        Package.installPackages(package='snowflake-connector-python', install_type="install --target={0}".format(libpath))

    # Refresh library caches and try to import again
    importlib.invalidate_caches()

import snowflake.connector

timing["imports"] = datetime.datetime.now()
print("{0} INFO: Snowflake connector imported in {1}".format(timing['start'],timing['imports']-timing['start']))

# COLLECT REQUIRED VARIABLES #
print("{0} INFO: Reading configuration data...".format(datetime.datetime.now()))
required_vars = ["ACCOUNT","DATABASE","PASSWORD","SCHEMA","USER","WAREHOUSE","TABLE","TEMP_PATH","FilePath"]
system_vars = ["ACCOUNT","PASSWORD","USER","WAREHOUSE"]
config_source = Alteryx.read("ConfigStream")
config = {}

for field in required_vars:
    if config_source[field][0]!=None:
        config[field] = config_source[field][0]
    elif field in system_vars:
        if os.getenv("SNOWFLAKE_"+field) != None:
            config[field] = os.getenv("SNOWFLAKE_"+field)
        else:
            print("SNOWFLAKE_{0} is not defined as a system variable and must be entered manually.\n".format(field))
            sys.exit()
    elif field == "TEMP_PATH":
        config[field] = os.path.abspath(os.getenv("TEMP"))
    else:
        print("{0} has not been entered.\n".format(field))
        sys.exit()
        
timing["config_checked"] = datetime.datetime.now()

# Capture Query to create table
query = Alteryx.read("#3")["SQL_CREATE_TABLE"][0].replace("%TABLE_NAME%",config["TABLE"])
print("{0} INFO: Config loaded in {1}".format(timing['config_checked'],timing['config_checked']-timing['imports']))
print("{0} INFO: Connecting to Snowflake with provided credentials...".format(datetime.datetime.now()))
###########################################################
con = snowflake.connector.connect(
  user=config["USER"],
  password=config["PASSWORD"],
  account=config["ACCOUNT"],
  warehouse=config["WAREHOUSE"],
  database=config["DATABASE"],
  schema=config["SCHEMA"]
)

timing["connection_established"] = datetime.datetime.now()
print("{0} INFO: Connected to Snowflake in {1}".format(timing['connection_established'],timing['connection_established']-timing['config_checked']))
print("{0} INFO: Activating schema and warehouse".format(datetime.datetime.now()))
# Set warehouse and schema
con.cursor().execute("USE WAREHOUSE {0}".format(config["WAREHOUSE"]))
con.cursor().execute("USE SCHEMA {0}.{1}".format(config["DATABASE"],config["SCHEMA"]))

timing["warehouse_schema_selected"] = datetime.datetime.now()
print("{0} INFO: Warehouse Selected in {1}".format(timing['warehouse_schema_selected'],timing['warehouse_schema_selected']-timing['connection_established']))

print("{0} INFO: Creating temp table...".format(datetime.datetime.now()))
# Execute Table Creation #
con.cursor().execute(query)
timing["table_created"] = datetime.datetime.now()
print("{0} INFO: Table created in {1}".format(timing['table_created'],timing['table_created']-timing['warehouse_schema_selected']))


# Putting Data
print("{0} INFO: Uploading data into temp table stage...".format(datetime.datetime.now()))
fname = config["TABLE"]
fpath = os.path.abspath(config["FilePath"])
sq_fpath = fpath.replace("\\","//")

try:
    con.cursor().execute("PUT 'file://{0}*' @%{1} PARALLEL=64".format(sq_fpath,config["TABLE"]))
except:
    pass # This is because of error "noise" with azure connections


timing["put_completed"] = datetime.datetime.now()

print("{0} INFO: Upload completed in {1}".format(timing['put_completed'],timing['put_completed']-timing['table_created']))
print("{0} INFO: Issuing copy command from stage into temp table...".format(datetime.datetime.now()))

try:
    con.cursor().execute("""COPY INTO {0} file_format = (TYPE=CSV FIELD_DELIMITER='|' COMPRESSION=GZIP SKIP_HEADER=1 FIELD_OPTIONALLY_ENCLOSED_BY='"')""".format(config["TABLE"]))
except:
    pass


timing["copy_completed"] = datetime.datetime.now()
print("{0} INFO: Copy completed in {1}".format(timing['copy_completed'],timing['copy_completed']-timing['put_completed']))

## Delete Temp Files ##
print("{0} INFO: Clearing temporary files...".format(datetime.datetime.now()))
filepath = config['FilePath']
directory = os.path.dirname(filepath+'.gz')
files = [directory +"\\" +f for f in os.listdir(directory) if filepath in directory +"\\" +f]


for f in files:
    os.remove(f)


timing["temp_files_cleared"] = datetime.datetime.now()
print("{0} INFO: Temporary files cleaned in {1}".format(timing['temp_files_cleared'],timing['temp_files_cleared']-timing['copy_completed']))
print("{0} INFO: Bulk upload process completed in {1}".format(timing['temp_files_cleared'],timing['temp_files_cleared']-timing['start']))