
from ayx import Package
from ayx import Alteryx
import os

filepath = Alteryx.read("#1")['FilePath'][0]
path = os.path.dirname(filepath)

if not os.path.exists(os.path.abspath(path)):
    os.makedirs(os.path.abspath(path))
    