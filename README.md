# Snowflake Bulk Loader Macro

This is an Alteryx macro implementation of bulk loading into Snowflake,
    using an incoming data stream and optionally piping the data out as an
    in-db data stream.
    
## You will need snowflake credentials:  
> Snowflake account: this is everything after https:// and before snowflakecomputing.com (looks like something.east-us-2.azure). See here for more: [Snowflake Docs: Your Snowflake Account](https://docs.snowflake.com/en/user-guide/connecting.html#your-snowflake-account-name)  
> Snowflake user: the username you will use for this upload.  
> Snowflake password: password for the given user.  
> Snowflake warehouse: name of the warehouse you will use in snowflake  

(Note) The above 4 settings can be configured as environment variables and left blank.
You must name them exactly as they appear in the config menu for the macro: (SNOWFLAKE_ACCOUNT, SNOWFLAKE_WAREHOUSE) etc...
It may be helpful to use environment variables if you're deploying this macro to different environments which use a different warehouse or user.
The Alteryx app Set_Credentials.yxwz is used to set credentials as your system variables (and can be set up as app in the Gallery to update these credentials on your gallery server without remote login.)

## Additionally, you will need:

> Snowflake database  
> Snowflake schema  
> Snowflake target table name  
> (Optional) temp path.  Should look like D:\foo\bar  with no '\' at the end.
    If a temp path is not provided, the macro will attempt to automatically discover the default temp path from your system variables.  
> (Optional) Connection Name. If you would like to continue with your table to in-db work, you will need to provide
    the name of an ODBC connection (The Alteryx connection name for snowflake which USES your odbc driver, not the ODBC manager connection name.)  
    If you do not provide this connection, please make sure 'Disable In-DB output' setting is checked.  

Feel free to share, use, and modify the macro as desired.  
If you need additional support, reach out to us at sales@keyrus.us


